<?php
/**
 * Front controller
 */

require __DIR__ . '/../vendor/autoload.php';
session_start();

$doNotDispatch = false;

try {
    $app    = new \App();
    $router = new \Router($app);
} catch (\Exception $ex) {
    $doNotDispatch = true;
    header('HTTP/1.0 500 Internal Server Error');
    $error = $ex->getMessage();
    include '../src/Views/500.phtml';
}
if(false === $doNotDispatch) {
    try {
        $router->findRouteAndDispatch();
    } catch (\Exception $ex) {
        header('HTTP/1.0 404 Not found');
        $error = $ex->getMessage();
        include '../src/Views/404.phtml';
    }
}



