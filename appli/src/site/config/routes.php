<?php

return [
    'homepage' => [
        'pattern'   => '^/$',
        'action' => [
            'controller' => 'AuthController',
            'action'     => 'login',
        ],
    ],
    'logout' => [
        'pattern'   => '^/logout$',
        'action' => [
            'controller' => 'AuthController',
            'action'     => 'logout',
        ],
    ],
    'messages' => [
        'pattern'   => '^/messages$',
        'action' => [
            'controller' => 'TchatController',
            'action'     => 'messages',
        ],
    ],
    'messagesupdate' => [
        'pattern'   => '^/messagesupdate/(\d+)$',
        'action' => [
            'controller' => 'TchatController',
            'action'     => 'messagesUpdate',
        ],
    ],
    'tchat' => [
        'pattern' => '^/tchat/(\d+)$',
        'action' => [
            'controller' => 'TchatController',
            'action'     => 'tchat',
        ],
    ],
    'sendmessage' => [
        'pattern' => '^/sendmessage$',
        'action' => [
            'controller' => 'TchatController',
            'action'     => 'sendMessage',
        ],
    ],

];
