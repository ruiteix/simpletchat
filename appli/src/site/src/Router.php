<?php

/**
 * Class Router
 *
 *
 * @package App
 */
class Router
{
    /**
     * @var |App
     */
    private $app;

    /**
     * Inject app so we can get rout configuration
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @throws \Exception
     */
    public function findRouteAndDispatch()
    {

        $path = '/';
        if (isset($_SERVER['REQUEST_URI'])) {
            $path = $_SERVER['REQUEST_URI'];
        }

        $routes = $this->app->getConfig('routes');

        foreach ($routes as $route) {
            $matches = [];
            if(preg_match('#' . $route['pattern'] . '#', $path, $matches)) {
                $ctrlName   = '\\Controllers\\' . $route['action']['controller'];
                $actionName = $route['action']['action'] . 'Action';
                $ctrl = new $ctrlName($this->app);
                if(method_exists($ctrl, $actionName)) {
                    $ctrl->{$actionName}($matches);
                    return;
                }

            }
        }
        throw new \Exception('Not Found');
    }
}
