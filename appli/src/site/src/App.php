<?php

use Db\PdoMysql;

/**
 * Class App
 * @package App
 */
class App
{
    /**
     * @var PdoMysql
     */
    private $db;
    /**
     * @var array
     */
    private $configs = [];

    /**
     * Load config, database, ...
     */
    public function __construct()
    {
        $this->loadConfigs();
        $this->db = new PdoMysql(
            $this->configs['database']['dsn'],
            $this->configs['database']['username'],
            $this->configs['database']['password']
        );
    }

    /**
     * Load one config file
     */
    private function loadConfig($anem)
    {
        $filepath = dirname(dirname(__FILE__)) . '/config/' . $anem. '.php';
        if(!file_exists($filepath)) {
            throw new \Exception('Cant load ' . $filepath);
        }

        $this->configs[$anem] = include $filepath;
    }

    /**
     * Load config/* in $this->configs
     */
    private  function loadConfigs()
    {
        $this->loadConfig('database');
        $this->loadConfig('routes');
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getConfig($name)
    {
        return !empty($this->configs[$name]) ? $this->configs[$name] : null;
    }

    /**
     * @return \Db\AbstractDb
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param $modelName
     * @return mixed
     */
    public function getModel($modelName)
    {
        $modelName   = '\\Models\\' . $modelName;
        return new $modelName($this->getDb());
    }
}
