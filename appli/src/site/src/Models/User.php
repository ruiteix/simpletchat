<?php

namespace Models;

class User extends BaseModel
{
    protected $table = 'users';

    protected $columns = [
        'username',
        'password',
        'created_at',
        'salt',
        'status',
    ];

    const STATUS_OFFLINE = 1;
    const STATUS_ONLINE  = 2;

    /**
     * @param int $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->abstractDb->query("
SELECT
    *
FROM 
  {$this->table} t
WHERE
    t.id = :id 
",
            [
                'id' => $id
            ]
        );
    }

    /**
     * @return mixed
     */
    public function create($username, $password)
    {
        $salt = $this->generateSalt();

        $this->abstractDb->query("
INSERT INTO
  {$this->table} (username, password, salt, created_at)
VALUES(:username, :password, :salt, NOW())",
            [
                'username' => $username,
                'password' => $this->encodePassword($password, $salt),
                'salt'     => $salt,
            ]
        );
        return $this->findBy(['username' => $username]);
    }

    /**
     * @return string
     */
    private function generateSalt()
    {
        return uniqid(mt_rand(), true);
    }

    /**
     * @param string $rawPassword
     * @param string $salt
     * @return bool|string
     */
    public function encodePassword($rawPassword, $salt)
    {
        return password_hash($rawPassword, PASSWORD_BCRYPT, [
            'cost' => 17,
            'salt' => $salt
        ]);
    }

    /**
     * @param string $rawPassword
     * @param string $encoded
     * @return bool
     */
    public function verifyPassword($rawPassword, $encoded)
    {
        return password_verify($rawPassword, $encoded);
    }
}
