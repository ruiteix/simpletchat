<?php

namespace Models;

class Message extends BaseModel
{
    protected $table = 'messages';

    /**
     *
     * Get all
     *
     * @param int $user_id
     * @param int $page
     * @param int $limit
     *
     * @return mixed
     */
    public function findAllByUser($user_id, $page = 1, $limit = 20)
    {
        return $this->abstractDb->query("
SELECT
    *
FROM
  {$this->table}
WHERE
  writer_id = :user_id
OR
  recipient_id = :user_id
ORDER BY 
    created_at DESC",
            [
                'user_id' => $user_id,
            ],
            [
                'offset' => ($page - 1) * $limit,
                'limit'  => $limit,
            ]);
    }

    /**
     * Get all messages sent or recieved from a recipient
     *
     * @param int $writer_id
     * @param int $recipient_id
     * @return mixed
     */
    public function findAllWithUsers($writer_id, $recipient_id)
    {
        return $this->abstractDb->query("
SELECT
    *
FROM
  {$this->table}
WHERE (writer_id = :user_id AND recipient_id = :recipient_id)
OR (recipient_id = :user_id AND writer_id = :recipient_id)
ORDER BY 
    created_at ASC",
            [
                'user_id'      => $writer_id,
                'recipient_id' => $recipient_id,
            ]);
    }

    /**
     * @param $writer_id
     * @param $recipient_id
     * @param $message
     */
    public function addMessage($writer_id, $recipient_id, $message)
    {
        $this->abstractDb->query("
INSERT INTO
  {$this->table} (writer_id, recipient_id, message, created_at)
VALUES(:writer_id, :recipient_id, :message, NOW())",
            [
                'writer_id'    => $writer_id,
                'recipient_id' => $recipient_id,
                'message'      => $message,
            ]
        );
    }
}
