<?php

namespace Models;

class BaseModel
{
    protected $abstractDb;
    protected $table;
    protected $columns = [];

    public function __construct($abstractDb)
    {
        $this->abstractDb = $abstractDb;
    }

    /**
     * @param $id
     * @param array $values
     * @return null
     * @throws \Exception
     */
    public function update($id, array $values)
    {
        if(!$this->table) {
            throw new \Exception('table cant be null');
        }
        $sets = [];
        foreach ($values as $key => $value) {
            if(in_array($key, $this->columns)) {
                $sets []= $key . ' = :' . $key;
            }
        }
        if(empty($sets)) {
            return null;
        }

        $this->abstractDb->query("
UPDATE
  {$this->table}
SET " . implode(',', $sets) ."
WHERE
    id = :id
LIMIT 1",
            array_merge($values, ['id' => $id])
        );
    }

    /**
     * Get all
     *
     * @return mixed
     * @todo : paginate
     */
    public function findAll()
    {
        return $this->abstractDb->query("
SELECT
    *
FROM
  {$this->table}
ORDER BY 
created_at DESC", []);
    }

    /**
     * @param array $filters
     * @throws \Exception
     * @return mixed
     */
    public function findBy(array $filters)
    {
        if(!$this->table) {
            throw new \Exception('table cant be null');
        }
        $wheres = [];
        foreach ($filters as $key => $value) {
            if(in_array($key, $this->columns)) {
                $wheres []= $key . ' = :' . $key;
            }
        }
        if(empty($wheres)) {
            return null;
        }

        return $this->abstractDb->query("
SELECT
    *
FROM
  {$this->table}
WHERE
    " . implode(' AND ', $wheres),
            $filters
        );
    }
}
