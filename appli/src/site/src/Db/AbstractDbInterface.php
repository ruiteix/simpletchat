<?php

namespace Db;

interface AbstractDbInterface
{
    /**
     * @param string $str
     * @return string mixed
     */
    public function quote($str);

    /**
     * @param string $sql
     * @param array $params
     * @param array $limit
     * @return mixed
     */
    public function query($sql, array $params, array $limit = null);
}
