<?php

namespace Db;

class PdoMysql implements AbstractDbInterface
{
    /**
     * @var \PDO
     */
    private $link;

    public function __construct($dsn, $username, $passwprd)
    {
        $this->link = new \PDO($dsn, $username, $passwprd);
    }

    /**
     * @inheritdoc
     */
    public function quote($str)
    {
        return $this->link->quote($str);
    }

    /**
     * @inheritdoc
     */
    public function query($sql, array $params, array $limit = null)
    {
        if(isset($limit['offset']) && isset($limit['limit'])) {
            $sql .= '
LIMIT :offset, :limit
';
        }
        $stmt = $this->link->prepare($sql);
        if(!$stmt) {
            throw new \Exception($this->link->errorInfo()[2]);
        }
        if(isset($limit['offset']) && isset($limit['limit'])) {
            $offsetValue = (int) $limit['offset'];
            $limitValue  = (int) $limit['limit'];
            $stmt->bindValue(':offset', $offsetValue , \PDO::PARAM_INT);
            $stmt->bindValue(':limit', $limitValue , \PDO::PARAM_INT);
        }

        foreach ($params as $key => $param) {
            $stmt->bindValue(':' . $key, $param , \PDO::PARAM_STR);
        }
        if(!$stmt->execute()) {
            throw  new \Exception($stmt->errorInfo()[2]);
        }
        $rows = [];
        foreach ($stmt as $row) {
            $rows []= $row;
        }
        return $rows;
    }

}
