<?php

namespace View;

class ViewHelpers
{
    /**
     *
     */
    public function head($parameters)
    {
        ?><!DOCTYPE html>
        <html>
        <head>
            <meta charset="UTF-8" />
            <!-- Latest compiled and minified CSS -->
            <?php $this->mainCss(); ?>
            <title>Welcome to SimpleTchat</title>
        </head>
        <body>
        <?php if(!empty($parameters['user']['username'])): ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/">Simpletchat</a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php $this->e(ucfirst($parameters['user']['username'])); ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/logout">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        <?php endif;
    }

    public function foot()
    {
        $this->mainJs();
        ?>
        </body>
        </html>
        <?php
    }

    public function mainCss()
    {
        ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <?php
    }

    public function mainJs()
    {
        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <?php
    }

    public function e($str)
    {
        echo htmlentities($str, ENT_QUOTES);
    }
}

$viewHelpers = new ViewHelpers;
