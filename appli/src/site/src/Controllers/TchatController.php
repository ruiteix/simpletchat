<?php

namespace Controllers;

class TchatController extends BaseController
{
    /**
     * Users list
     * @todo : paginate
     */
    public function messagesAction()
    {
        $user = $this->getUser();
        if(!$user) {
            // user not logged
            $this->redirect('/');
            return;
        }

        $this->renderView('messages.phtml', [
            'user'     => $user[0],
            'messages' => $this->getModel('Message')->findAllByUser($user[0]['id']),
            'users'    => $this->getModel('User')->findAll(),
        ]);
    }

    /**
     * @param array $params
     */
    public function tchatAction($params)
    {
        $recipientId = isset($params[1]) && (int) $params[1] > 0 ? $params[1] : null;
        if(!$recipientId) {
            $this->error(404, 'Not found');
            return;
        }

        $user = $this->getUser();
        if(!$user) {
            // user not logged
            $this->redirect('/');
            return;
        }
        $userModel = $this->getModel('User');
        $recipient = $userModel->findById($recipientId);
        if(empty($recipient)) {
            $this->error(404, 'Not found');
            return;
        }

        $messageModel = $this->getModel('Message');

        $this->renderView('tchat.phtml', [
            'user'      => $user[0],
            'recipient' => $recipient[0],
            'messages'  => $messageModel->findAllWithUsers($user[0]['id'], $recipientId)
        ]);
    }

    /**
     *
     */
    public function sendMessageAction()
    {
        $user = $this->getUser();
        if(!$user) {
            // user not logged
            $this->error(404, 'Not found');
            return;
        }
        $recipientId = isset($_POST['id']) ? $_POST['id'] : null;

        if(!$recipientId) {
            $this->error(404, 'Not found');
            return;
        }

        $userModel = $this->getModel('User');
        $recipient = $userModel->findById($recipientId);
        if(empty($recipient)) {
            $this->error(404, 'Not found');
            return;
        }
        $message = isset($_POST['message']) ? $_POST['message'] : '';
        if(empty($message)) {
            $this->error(400, 'Bad request');
            return;
        }
        $message = filter_var($message, FILTER_SANITIZE_STRING);

        $messageModel = $this->getModel('Message');
        $messageModel->addMessage($user[0]['id'], $recipient[0]['id'], $message);

        header('Content-Type: application/json');
        echo json_encode([
            'status'  => 'ok',
            'date'    => date('Y-m-d H:i:s'),
            'message' => $message,
        ]);
    }


    /**
     * @param array $params
     */
    public function messagesUpdateAction($params)
    {
        $recipientId = isset($params[1]) && (int) $params[1] > 0 ? $params[1] : null;
        if(!$recipientId) {
            $this->error(404, 'Not found');
            return;
        }

        $user = $this->getUser();
        if(!$user) {
            // user not logged
            $this->redirect('/');
            return;
        }
        $userModel = $this->getModel('User');
        $recipient = $userModel->findById($recipientId);
        if(empty($recipient)) {
            $this->error(404, 'Not found');
            return;
        }

        $messageModel = $this->getModel('Message');

        header('Content-Type: application/json');
        echo json_encode([
            'status' => 'ok',
            'messages' => $messageModel->findAllWithUsers($user[0]['id'], $recipientId)
        ]);
    }
}
