<?php

namespace Controllers;

use Models\BaseModel;

class BaseController
{
    /**
     * @var \App
     */
    private $app;

    /**
     *
     * @param \App $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @param string $view
     * @param array $parameters
     * @throws \Exception
     */
    protected function renderView($view, $parameters)
    {
        $filename = __DIR__ . '/../Views/' . $view;

        if(!file_exists($filename)) {
            throw new \Exception('View not found ' . $filename);
        }
        include $filename;
    }

    /**
     * @param string $to url
     * @param string $httpResponseCode
     */
    protected function redirect($to, $httpResponseCode = '302')
    {
        if(!empty($to)) {
            header('Location: ' . $to, true, $httpResponseCode);
        }
    }

    /**
     * @param string $modelName
     * @return BaseModel
     */
    protected function getModel($modelName)
    {
        return $this->app->getModel($modelName);
    }

    /**
     * Generate a forms csrf token
     *
     * @return string
     */
    protected function getCsrfToken()
    {
        $token = uniqid(rand(), true);
        $_SESSION['csrfToken'] = $token;
        $_SESSION['csrfTokenTime'] = time();
        return $token;
    }

    /**
     * @param string $token
     * @return bool
     */
    protected function isCsrfTokenValide($token)
    {
        if(empty($token)
            || empty($_SESSION['csrfToken'])
            || empty($_SESSION['csrfTokenTime'])
        ) {
            return false;
        }

        if($token != $_SESSION['csrfToken']) {
            return false;
        }
        if(!($_SESSION['csrfTokenTime'] >= time() - ( 10 * 60 ) )) {

        }

        $referer = str_replace(['http://', 'https://'], ['', ''], $_SERVER['HTTP_REFERER']);

        return (0 === strpos($referer, $_SERVER['HTTP_HOST']));
    }

    /**
     * Return logged user
     */
    protected function getUser()
    {
        if(!isset($_SESSION['user_id'])
            || empty($_SESSION['logged'])
            || false === $_SESSION['logged']) {
            return null;
        }

        return $this->getModel('User')
            ->findById($_SESSION['user_id']);
    }

    /**
     * Log user
     *
     * @param int $user_id
     */
    protected function loginUser($user_id)
    {
        $_SESSION['user_id'] = $user_id;
        $_SESSION['logged'] = true;
    }

    /**
     * @param int $code
     * @param string $message
     */
    protected function error($code, $message)
    {
        header('HTTP/1.0 ' . $code . ' ' . $message);
    }
}
