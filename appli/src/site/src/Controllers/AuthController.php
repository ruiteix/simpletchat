<?php

namespace Controllers;

class AuthController extends BaseController
{
    /**
     * Login form : log or create a user
     */
    public function loginAction()
    {
        $user   = $this->getUser();
        $errors = [];
        if($user) {
            // user logged
            $this->redirect('/messages');
            return;
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $csrfToken  = !empty($_POST['csrf-token']) ? $_POST['csrf-token'] : null;
            if(!$this->isCsrfTokenValide($csrfToken)) {
                $errors[] = 'Please try again';
            }
            $username = !empty($_POST['_username']) ? $_POST['_username'] : null;
            if(!$username) {
                $errors[] = 'Username can not be empty';
            } else {
                $username = filter_var($username, FILTER_SANITIZE_STRING);
            }

            // No need to sanitize password => hash
            $password = !empty($_POST['_password']) ? $_POST['_password'] : null;
            if(!$password) {
                $errors[] = 'Password can not be empty';
            }

            if(empty($errors)) {
                $userModel = $this->getModel('User');
                $result = $userModel->findBy([
                    'username' => $username
                ]);
                $user = null;

                // User exists => check password
                if(!empty($result)) {
                    $user = $result[0];
                    if(!$userModel->verifyPassword($password, $user['password'])) {
                        $errors [] = 'Invalid credentials';
                    }
                } else { // create user
                    $result = $userModel->create($username, $password);
                    $user = $result[0];
                }
                if(empty($errors)) {
                    $this->loginUser($user['id']);
                    $this->redirect('/messages');
                    return;
                }
            }
        }
        $this->renderView('login.phtml', [
            'csrfToken' => $this->getCsrfToken(),
            'errors'    => $errors,
        ]);
    }

    /**
     *
     */
    public function logoutAction()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['logged']);
        $this->redirect('/');
    }
}
