# Livechat

## Installation

* Install docker
* Add *127.0.0.1 simpletchat.local* in /etc/hosts
* In this shell build containers
```
#!bash
docker-compose build

```
* After build, execute containers :
```
#!bash
docker-compose up -d
```
* In the shell connect to the container:
```
#!bash
docker exec -it simpletchat_appli_1 bash
```
* In container
```
#!bash
cd /var/www/html/site && composer update
```
* Connect mysql at localhost:3312 with  root/simpletchat and execute thoses sql queries :

```
#!sql
CREATE TABLE IF NOT EXISTS `simpletchat`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `salt` VARCHAR(255) NOT NULL,
  `status` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `simpletchat`.`messages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `writer_id` INT UNSIGNED NOT NULL,
  `recipient_id` INT UNSIGNED NOT NULL,
  `message` VARCHAR(512) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_messages_writer_id_idx` (`writer_id` ASC),
  INDEX `FK_messages_recipient_id_idx` (`recipient_id` ASC),
  CONSTRAINT `FK_messages_writer_id`
    FOREIGN KEY (`writer_id`)
    REFERENCES `simpletchat`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_messages_recipient_id`
    FOREIGN KEY (`recipient_id`)
    REFERENCES `simpletchat`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB;

ALTER TABLE `simpletchat`.`messages` 
ADD COLUMN `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `message`;

```

* Go to http://simpletchat.local:92/ & enjoy (I hope)

## Features
* Message/user pagination
* Writer/Recipient names in messages page
* 404/500 page
* Users live status
